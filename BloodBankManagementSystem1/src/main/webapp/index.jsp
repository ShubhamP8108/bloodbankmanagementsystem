<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<link  href="/cssresources/index.css"  rel="stylesheet" type="text/css"> 
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<style type="text/css">
	body {
	/* background:url('https://wallpaperaccess.com/full/2982045.jpg'); */
	background-color:white; 
	padding: 50px;
}

#login-dp {
	min-width: 250px;
	padding: 14px 14px 0;
	overflow: hidden;
	background-color: rgba(255, 255, 255, .8);
}

#login-dp .help-block {
	font-size: 12px
}

#login-dp .bottom {
	background-color: rgba(255, 255, 255, .8);
	border-top: 1px solid #ddd;
	clear: both;
	padding: 14px;
}

#login-dp .social-buttons {
	margin: 12px 0
}

#login-dp .social-buttons a {
	width: 49%;
}

#login-dp .form-group {
	margin-bottom: 10px;
}

.btn-fb {
	color: #fff;
	background-color: #3b5998;
}

.btn-fb:hover {
	color: #fff;
	background-color: #496ebc
}

.btn-tw {
	color: #fff;
	background-color: #55acee;
}

.carousel-inner>.item>img, .carousel-inner>.item>a>img {
	width: 70%;
	margin: auto;
}

.btn-tw:hover {
	color: #fff;
	background-color: #59b5fa;
}

@media ( max-width :768px) {
	#login-dp {
		background-color: inherit;
		color: #fff;
	}
	#login-dp .bottom {
		background-color: inherit;
		border-top: 0 none;
	}
}

@import url(http://fonts.googleapis.com/css?family=Roboto);

/****** LOGIN MODAL ******/
.loginmodal-container {
	padding: 30px;
	max-width: 350px;
	width: 100% !important;
	background-color: #F7F7F7;
	margin: 0 auto;
	border-radius: 2px;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	overflow: hidden;
	font-family: roboto;
}

.loginmodal-container h1 {
	text-align: center;
	font-size: 1.8em;
	font-family: roboto;
}

.loginmodal-container input[type=submit] {
	width: 100%;
	display: block;
	margin-bottom: 10px;
	position: relative;
}

.loginmodal-container input[type=text], input[type=password] {
	height: 44px;
	font-size: 16px;
	width: 100%;
	margin-bottom: 10px;
	-webkit-appearance: none;
	background: #fff;
	border: 1px solid #d9d9d9;
	border-top: 1px solid #c0c0c0;
	/* border-radius: 2px; */
	padding: 0 8px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
}

.loginmodal-container input[type=text]:hover, input[type=password]:hover
	{
	border: 1px solid #b9b9b9;
	border-top: 1px solid #a0a0a0;
	-moz-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
	-webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
	box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
}

.loginmodal {
	text-align: center;
	font-size: 14px;
	font-family: 'Arial', sans-serif;
	font-weight: 700;
	height: 36px;
	padding: 0 8px;
	/* border-radius: 3px; */
	/* -webkit-user-select: none;
  user-select: none; */
}

.loginmodal-submit {
	/* border: 1px solid #3079ed; */
	border: 0px;
	color: #fff;
	text-shadow: 0 1px rgba(0, 0, 0, 0.1);
	background-color: #4d90fe;
	padding: 17px 0px;
	font-family: roboto;
	font-size: 14px;
	/* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#4787ed)); */
}

.loginmodal-submit:hover {
	/* border: 1px solid #2f5bb7; */
	border: 0px;
	text-shadow: 0 1px rgba(0, 0, 0, 0.3);
	background-color: #357ae8;
	/* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#357ae8)); */
}

.loginmodal-container a {
	text-decoration: none;
	color: #666;
	font-weight: 400;
	text-align: center;
	display: inline-block;
	opacity: 0.6;
	transition: opacity ease 0.5s;
}

.login-help {
	font-size: 12px;
}</style>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<div class="col-md-1"></div>
				<img src="images/logo.jpg">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <img
						src="images/logo.jpg" width="100" height="50" alt="logo"> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>

			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">

				<a class="navbar-brand" href="#"><b>Blood Bank
						Management System</b></a>
				<div class="col-md-1"></div>
				<ul class="nav navbar-nav">
					<li><a href="#">About Us</a></li>
					<li><a href="#">Contact Us</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li><a href="login"><b>Login/Sign in here</b></a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
	</nav>
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
			<li data-target="#myCarousel" data-slide-to="3"></li>
			<li data-target="#myCarousel" data-slide-to="4"></li>
			<li data-target="#myCarousel" data-slide-to="5"></li>
			<li data-target="#myCarousel" data-slide-to="6"></li>
		</ol>
 
		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="fill">
					<img src="images/homeimg3.jpg" alt="bloodBank1" width="1600"
						height="400" align="center">
				</div>
			</div>
			<div class="item">
				<div class="fill">
					<img src="images/homeimg4.jpg" alt="bloodBank2" width="1600"
						height="400" align="center">
				</div>
			</div>

			<div class="item">
				<div class="fill">
					<img src="images/homeimg5.jpg" alt="bloodBank3" width="1600"
						height="400" align="center">
				</div>
			</div>

			<div class="item">
				<div class="fill">
					<img src="images/homeimg6.jpg" alt="bloodBank4" width="1600"
						height="400" align="center">
				</div>
			</div>
			<div class="item">
				<div class="fill">
					<img src="images/homeimg7.jpg" alt="bloodBank4" width="1600"
						height="400" align="center">
				</div>
			</div>
			<div class="item">
				<div class="fill">
					<img src="images/homeimg8.jpg" alt="bloodBank4" width="1600"
						height="400" align="center">
				</div>
			</div>
			<div class="item">
				<div class="fill">
					<img src="images/homeimg9.jpg" alt="bloodBank4" width="1600"
						height="400" align="center">
				</div>
			</div>
		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" role="button"
			data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"
			aria-hidden="true"></span> <span class="sr-only">Previous</span>
		</a> <a class="right carousel-control" href="#myCarousel" role="button"
			data-slide="next"> <span
			class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	<div class="container">

		<!-- Marketing Icons Section -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Welcome to Blood Bank Management System
				</h1>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>
							<i class="fa fa-fw fa-check"></i> Know Your Type
						</h4>
					</div>
					<div class="panel-body">
						<p>Are you A? B? O? AB?If you want to become a blood donor,
							find out your blood type! The easiest way to accurately, safely
							and cost-effectively discover your blood type is to
							donate.Whatever your blood type, there is a donation type
							specific to you.</p>
						<a href="#" class="btn btn-default">Learn More</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>
							<i class="fa fa-fw fa-gift"></i> Ethnicity &amp; Blood Donation
						</h4>
					</div>
					<div class="panel-body">
						<p>Less than 1 percent of blood donations are from African
							Americans or Hispanics.Though compatibility is not based on race,
							genetically similar blood is best for patients who need repeated
							or large volumes of blood transfusions.</p>
						<a href="#" class="btn btn-default">Learn More</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>
							<i class="fa fa-fw fa-compass"></i> Where to Donate
						</h4>
					</div>
					<div class="panel-body">
						<p>We operate 22 conveniently located community donor centers
							and fixed sites — all of our donor centers and fixed sites
							collect whole blood, and many of the sites also offer the
							automated and apheresis donation processes.</p>
						<a href="#" class="btn btn-default">Learn More</a>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->

		<!-- Portfolio Section -->
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">Information Center</h2>
			</div>
			<div class="col-md-4 col-sm-6">
				<a href="portfolio-item.html"> <img
					class="img-responsive img-portfolio img-hover"
					src="images/pfimg1.jpg" alt="">
				</a>
			</div>
			<div class="col-md-4 col-sm-6">
				<a href="portfolio-item.html"> <img
					class="img-responsive img-portfolio img-hover"
					src="images/pfimg2.jpg" alt="">
				</a>
			</div>
			<div class="col-md-4 col-sm-6">
				<a href="portfolio-item.html"> <img
					class="img-responsive img-portfolio img-hover"
					src="images/pfimg3.jpg" alt="">
				</a>
			</div>

		</div>
		<!-- /.row -->

		<!-- Features Section -->
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">Blood facts</h2>
			</div>
			<div class="col-md-6">
				<ul>
					<li>Blood is the life-maintaining fluid that circulates
						through the body's heart, arteries, veins and capillaries.</li>
					<li>Blood carries to the body nourishment, electrolytes,
						hormones, vitamins, antibodies, heat, and oxygen.</li>
					<li>Blood carries away from the body waste matter and carbon
						dioxide.</li>
					<li>Blood fights against infection and helps heal wounds,
						keeping you healthy.</li>
					<li>Blood makes up about 7% of your body's weight.</li>
					<li>A newborn baby has about one cup of blood in his or her
						body.</li>
					<li>White blood cells are the body's primary defence against
						infection.</li>
					<li>Granulocytes, a type of white blood cell, roll along blood
						vessel walls to search and destroy bacteria.</li>
					<li>Red blood cells carry oxygen to the body's organs and
						tissues.</li>
					<li>There are about one billion red blood cells in two to
						three drops of blood.</li>
				</ul>
			</div>
			<div class="col-md-6">
				<img class="img-responsive" src="images/subimg1.jpg" alt="">
			</div>
		</div>
		<!-- /.row -->

		<hr>

		<!-- Call to Action Section -->

		<div class="col-md-2">
			<a href="#">About us</a>
		</div>
		<div class="col-md-2">
			<a href="#">Feedback</a>
		</div>
		<div class="col-md-2">
			<a href="#">FAQ</a>
		</div>
		<div class="col-md-2">
			<a href="#">Privacy Policy</a>
		</div>
		<div class="col-md-2">
			<a href="#">Media Centre</a>
		</div>
		<div class="col-md-2">
			<a href="#">Share Your Experience</a>
		</div>



		<hr>

		<!-- Footer -->
		<footer>
			<div class="row">
				<div class="col-lg-12">
					<p>Copyright &copy; bloodbank</p>
				</div>
			</div>
		</footer>

	</div>
</body>
</html>
