<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
body {
	background-image: url("images/bg3.jpg");
	margin: 0;
	padding: 0;
	background-size: cover;
	backgorund-position: center;
	font-family: sans-serif;
}

.loginBox {
	width: 320px;
	height: 420px;
	background: rgba(0, 0, 0, 0.5);
	color: #fff;
	top: 50%;
	left: 50%;
	position: absolute;
	transform: translate(-50%, -50%);
	box-sizing: border-box;
}

h3 {
	text-align: center;
	font-family: sans-serif;
}

a {
	font-size: 20px;
	color: "white";
	text-decoration: none;
}
</style>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="loginBox">
		<h3>Login Form</h3>
		<br>
		<form method="post">
			<div class="form-group">
				<p>
					User Name<span class="glyphicon glyphicon-user"></span>
				</p>
				<input type="text" name="username" class="form-control"
					placeholder="enter your name">
			</div>
			<div class="form-group">
				<p>
					Password <span class="glyphicon glyphicon-lock"></span>
				</p>
				<input type="password" name="password" class="form-control"
					placeholder="enter your password">
			</div>
			<div class="form-group">
				<input type="checkbox" name="remember"> <label
					for="remember">Remeber me</label> <a href="#" id="lin"
					style="color: white">Forgot password</a>
			</div>
			<br>
			<div class="form-group">
				<input type="submit" class="btn btn-primary btn-block active"
					class="form-control" value="Login">
			</div>
			<p>
				Don't have account yet? <a href="register" style="color: white">Register
					now</a>
			</p>
		</form>
	</div>

</body>
</html>
