package com.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class WebxmlConfiguration extends AbstractAnnotationConfigDispatcherServletInitializer 
 {
	
	protected Class<?>[] getRootConfigClasses() {
		
		return new Class[] { InternalResourceDatabaseConfiguration.class};
	}

	
	protected Class<?>[] getServletConfigClasses() {
		
		return new Class[] { };
	}

	
	protected String[] getServletMappings() {
		
		return new String[] {
				"/"
		};
	}
}



