<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
body {
	background-image: url("images/bg.png");
}

#divcol {
	background-color: black;
	color: white;
}

#spanid {
	font-size: 25px;
}

.avatar {
	position: absolute;
	width: 80px;
	height: 80px;
	border-radius: 50%;
	overflow: hidden;
	top: calc(-5%/ 2);
	left: calc(50% - 40px);
}

.container {
	min-width: 5%;
}

h3 {
	color: white;
	position: absolute;
	top: -15%;
	font-family: inherit;
}

label {
	color: white;
}
</style>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<nav class="navbar navbar-inverse">
	<div class="container-fluid" id="divcol">
		<div class="navbar-header">
			<!-- <a class="navbar-brand" href="#">BloodBank Management</a> -->
			<span id="spanid" class="navbr-brand">BloodBank Management</span>
		</div>
	</div>
	</nav>
	<div class="container-fluid p-5 col-9 my-3 ">
		<form class="col-lg-6 offset-lg-3" method="post">
			<h3>Registration Form</h3>
			<br> <br> <img src="images/img1.jpg" alt="" class="avatar">
			<div class="form-group">
				<strong><label for="username">FirstName:</label></strong> <input
					type="text" class="form-control" name="username"
					placeholder="enter your name">
			</div>
			<div class="form-group">
				<strong><label for="LastName">LastName:</label></strong> <input
					type="text" class="form-control" name="username"
					placeholder="enter your Lastname">
			</div>
			<div class="form-group">
				<strong><label for="BloodGroup">BloodGroup:</label></strong> <select
					class="form-control">
					<option>--Select Blood Group--</option>
					<option>A1+</option>
					<option>A1-</option>
					<option>A2+</option>
					<option>A2-</option>
					<option>B+</option>
					<option>B-</option>
					<option>A1B+</option>
					<option>A1B-</option>
					<option>AB+</option>
					<option>AB-</option>
					<option>O+</option>
					<option>O-</option>
					<option>A+</option>
					<option>A-</option>
					<option>Bombay group</option>
				</select>
			</div>
			<div class="form-group">
				<strong><label for="city">City:</label><strong> <input
						type="text" class="form-control" name="password"
						placeholder="enter your Location">
			</div>
			<input type="submit" class="btn btn-primary">
		</form>
	</div>
</body>
</html>